(function() {
  'use strict';
  var ListUserController = function($scope, $http, $location, $route, users, auth) {
    var onSuccessGetUsers = function (data) {
      $scope.users = data.data.data.users;
      $scope.total = data.data.data.total;
      if ($scope.total <= $scope.limit) {
        $scope.nextButtonDisable = 'disabled';
        $scope.previousButtonDisable = 'disabled';
      }
    }
    var onErrorGetUsers = function (data) {
      alert("Error: " + data.data.message);
    }
    $scope.limit = 10;
    $scope.offset = 0;
    $scope.nextButtonDisable = '';
    $scope.previousButtonDisable = 'disabled';
    users.getUsers($scope.limit, $scope.offset, onSuccessGetUsers, onErrorGetUsers);

    $scope.is_authenticated = auth.checkAuth();

    $scope.logout = function () {
      if (confirm("Do you want to logout?") == true) {
          auth.logout();
          $location.path("/login");
      }
      return false;
    };
    $scope.nextPage = function () {
      var tmp_offset = $scope.limit + $scope.offset;
      if (tmp_offset < $scope.total) {
        $scope.offset = tmp_offset
        if ($scope.user_search != '') {
          users.searchUsers($scope.user_search, $scope.limit, $scope.offset, onSuccessGetUsers, onErrorGetUsers);
        } else {
          users.getUsers($scope.limit, $scope.offset, onSuccessGetUsers, onErrorGetUsers);
        }
        $scope.previousButtonDisable = '';
      } else {
        $scope.nextButtonDisable = 'disabled';
        return false;
      }
      
    };
    $scope.previousPage = function () {
      var tmp_offset = $scope.offset - $scope.limit;
      if ($scope.offset >= 0) {
        $scope.offset = tmp_offset;
        if ($scope.user_search != '') {
          users.searchUsers($scope.user_search, $scope.limit, $scope.offset, onSuccessGetUsers, onErrorGetUsers);
        } else {
          users.getUsers($scope.limit, $scope.offset, onSuccessGetUsers, onErrorGetUsers);
        }
        
        $scope.nextButtonDisable = '';
      } else {
        $scope.previousButtonDisable = 'disabled';
        return false;
      }
    };

    $scope.search = function () {
        users.searchUsers($scope.user_search, $scope.limit, $scope.offset, onSuccessGetUsers, onErrorGetUsers);
    };
  };
  angular.module('users_management.list_user', [
    'ngRoute',
    'users_management.user_service',
    'users_management.auth_service'
  ])

  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/', {
      templateUrl: 'components/list_user/list_user.html',
      controller: 'ListUserController',
      loginRequire: true
    });
  }])
  
  .controller('ListUserController', ['$scope', '$http', '$location', '$route', 'users', 'auth', ListUserController]);
}());