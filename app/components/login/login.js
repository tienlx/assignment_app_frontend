(function() {
	'use strict';
	var LoginController = function($scope, $http, $routeParams, $location, auth) {
		var onSuccess = function (response) {
			auth.setAccessToken(response.data.access_token);
			auth.setAuth();
			$location.path('/');
		};

		var onError = function (response) {
			var error_str = response.data.error_description;
	        alert("Error: " + error_str);
		};

		$scope.login = function () {
			var login_info = {
				'grant_type': 'password',
				'client_id': 'b394fdac898be03cd0533f583a8b104c8e91f0ceeaa22008df5514d59a7e762b',
				'client_secret': '3d565a5222509ba1ced6c24461aa174811bb7e207c32a41c86883f8f3d4064a7',
				'username': $scope.username,
				'password': $scope.password,
			}
			auth.login(login_info, onSuccess, onError);
		};
	};
	angular.module('users_management.login', ['ngRoute'])

	.config(['$routeProvider', function($routeProvider) {
		$routeProvider.when('/login', {
			templateUrl: 'components/login/login.html',
			controller: 'LoginController'
		});
	}])

	.controller('LoginController', [ '$scope', '$http', '$routeParams', '$location', 'auth', LoginController]);
})();