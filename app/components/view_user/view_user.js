(function() {
'use strict';
  var ViewUserController = function($scope, $http, $routeParams, $location, users, auth) {
    var
      user_id = $routeParams.user_id,
      onSuccessFindUser = function (data) {
        $scope.user_info = data.data.data;
      },
      onErrorFindUser = function (data) {
        alert("Error: " + data.data.message);
      };
    users.findUser(user_id, onSuccessFindUser, onErrorFindUser);

  };
  angular.module('users_management.view_user', ['ngRoute', 'users_management.user_service'])

  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/view/:user_id', {
      templateUrl: 'components/view_user/view_user.html',
      controller: 'ViewUserController',
      loginRequire: true
    });
  }])
  
  .controller('ViewUserController', ['$scope', '$http', '$routeParams', '$location', 'users', 'auth', ViewUserController]);
}());