(function() {
  'use strict';
  var UsersService = function($http, auth) {
    var host = 'http://localhost:3000';
    var onDefaultError = function (data) {
        var error_str = '';
        angular.forEach(data.data.message, function(value, key){
          error_str = error_str + '\n' + key + ": " + value;
        });
        alert("Error: " + error_str);
      };

    var getUsers = function(limit, offset, onSuccess, onError) {
      $http({
        method: 'GET',
        url: host + '/users' + '?access_token=' + auth.getAccessToken() 
                  + '&limit=' + limit + '&offset=' + offset
      }). then (onSuccess, onError);
    };

    var findUser = function(user_id, onSuccess, onError) {
      $http({
        method: 'GET',
        url: host + '/users/' + user_id + '?access_token=' + auth.getAccessToken()
      }). then (onSuccess, onError);
    };

    var searchUsers = function (search_str, limit, offset, onSuccess, onError) {
      $http({
        method: 'POST',
        url: host + '/users/search' + '?access_token=' + auth.getAccessToken() 
                  + '&limit=' + limit + '&offset=' + offset,
        data: {'search_str': search_str}
      }). then (onSuccess, onError);
    }

    return {
      getUsers: getUsers,
      findUser: findUser,
      searchUsers: searchUsers
    };
  };
  angular.module('users_management.user_service', ['users_management.auth_service'])
  .service('users', ['$http', 'auth',  UsersService]);
}());