(function() {
  'use strict';
  var app = angular.module('users_management', [
    'ngRoute',
    'users_management.list_user',
    'users_management.view_user',
    'users_management.login'
  ]);
  
  app.config(function($routeProvider) {
    $routeProvider
      .otherwise({
        redirectTo: '/'
      });
  });
  app.run(function($rootScope, $location, auth) {
    $rootScope.$on('$routeChangeStart', function(event, next) {
        if(next.loginRequire && !auth.checkAuth()){
            $location.path('/login');
        }
      });
  });

}());