# README

Assignment for Ruby developer

Build sample web application as below:

* Login page

* Member directory
    * Members directory: all members listed, by default in alphabetical order. Member information is sample data.
    * Search members. Based on the following associated data: Name, Region, Interests, Specialism
    * Pagination

Non-functional requirements:

* Technologies 
   * Backend 
        * Ruby on Rails 5
        * PostgreSQL 
   * Frontend
        * AngularJS 1
        * Grunt 

* Well designed, unit testable

* Unit Test + Automation Test with 70% code coverage 

* Apply automation deployment


## Build App Command
````
$ npm install -g yo grunt-cli bower
$ npm install
$ bower install
$ gem install compass //Fix compass missing issue, need to have Ruby first
$ grunt
$ grunt serve
````
